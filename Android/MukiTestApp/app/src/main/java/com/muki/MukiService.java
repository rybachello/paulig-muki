package com.muki;
import static java.util.concurrent.TimeUnit.*;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.muki.core.MukiCupApi;
import com.muki.core.MukiCupCallback;
import com.muki.core.model.Action;
import com.muki.core.model.DeviceInfo;
import com.muki.core.model.ErrorCode;
import com.muki.core.model.ImageProperties;
import com.muki.core.util.ImageUtils;

import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by denis on 25.11.16.
 */
public class MukiService extends Service {
    private static final String TAG = "MyService";
    private static final String LOG_TAG = "MukiService";
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    public void beepForAnHour() {
//        final Runnable beeper = new Runnable() {
//            public void run() { System.out.println("beep"); }
//        };
//        final ScheduledFuture<?> beeperHandle =
//                scheduler.scheduleAtFixedRate(beeper, 0, 20, SECONDS);
//        scheduler.schedule(new Runnable() {
//            public void run() { beeperHandle.cancel(true); }
//        }, 60 * 60, SECONDS);
    }


    private MukiCupApi mMukiCupApi;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public void onDestroy() {
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onStart(Intent intent, int startid) {
        mMukiCupApi = new MukiCupApi(getApplicationContext(), new MukiCupCallback() {
            @Override
            public void onCupConnected() {
                Log.i(LOG_TAG, "Cup connected");
            }

            @Override
            public void onCupDisconnected() {
                    Log.i(LOG_TAG,"Cup disconnected");
            }

            @Override
            public void onDeviceInfo(DeviceInfo deviceInfo) {
                Log.i(LOG_TAG, deviceInfo.toString());
            }

            @Override
            public void onImageCleared() {
                    Log.i(LOG_TAG,"Image cleared");
            }

            @Override
            public void onImageSent() {
                    Log.i(LOG_TAG,"Image sent");
            }

            @Override
            public void onError(Action action, ErrorCode errorCode) {
                    Log.i(LOG_TAG,"Error:" + errorCode + " on action:" + action);
            }
        });

        final Runnable imageSender = new Runnable() {
            public void run() {
                System.out.println("start sending image");
                request();
                //todo send real image
            }
        };
        scheduler.scheduleAtFixedRate(imageSender, 0, 20, SECONDS);

    }

    public void request() {
       final String serialNumber ="0003771";

        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... strings) {
                String id;
                try {
                    String serialNumber = strings[0];
                    id = MukiCupApi.cupIdentifierFromSerialNumber(serialNumber);

                } catch (Exception e) {
                    id = "PAULIG_MUKI_466741";
                    e.printStackTrace();
                }
                return id;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                AsyncTask<String, Void, Bitmap> loadImage = new AsyncTask<String, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(String... params) {
                        String url = params[0];
                        URL urlUrl;
                        Bitmap image;
                        try {
                            urlUrl = new URL(url);
                            image  = BitmapFactory.decodeStream(urlUrl.openConnection().getInputStream());

                        } catch (Exception e) {
                            Log.e("DENIS_TAG", e.getMessage() + e.getStackTrace());
                            image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
                        }
                        return image;
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        super.onPostExecute(bitmap);
                        Bitmap mImage = ImageUtils.scaleBitmapToCupSize(bitmap);
                        int mContrast = ImageProperties.DEFAULT_CONTRACT;
                        setupImage(mImage, mContrast);
                        bitmap.recycle();
                        Log.d(LOG_TAG, Thread.currentThread().getName());
                        mMukiCupApi.sendImage(mImage, new ImageProperties(mContrast), serialNumber);
                    }
                };
                loadImage.execute("http://smartair.tech/resources/images/common/leave.png");

            }
        }.execute(serialNumber);
    }

    private void setupImage(final Bitmap mImage,final int mContrast) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap result = Bitmap.createBitmap(mImage);
                ImageUtils.convertImageToCupImage(result, mContrast);
                return result;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {

            }
        }.execute();
    }

}