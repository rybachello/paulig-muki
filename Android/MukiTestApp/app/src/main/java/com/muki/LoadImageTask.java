package com.muki;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by denis on 25.11.16.
 */
public class LoadImageTask extends AsyncTask<String,Void,Void> {


    @Override
    protected Void doInBackground(String... params) {
        String url = params[0];
        URL urlUrl = null;
        Bitmap image;
        try {
            urlUrl = new URL(url);
            image  = BitmapFactory.decodeStream(urlUrl.openConnection().getInputStream());

        } catch (Exception e) {
            Log.e("DENIS_TAG", e.getMessage() + e.getStackTrace());
           // image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
        }

        return null;
    }
}
