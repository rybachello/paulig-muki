package com.muki;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.muki.core.MukiCupApi;
import com.muki.core.MukiCupCallback;
import com.muki.core.model.Action;
import com.muki.core.model.DeviceInfo;
import com.muki.core.model.ErrorCode;
import com.muki.core.model.ImageProperties;
import com.muki.core.util.ImageUtils;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MainActivity extends AppCompatActivity {

    private EditText mSerialNumberEdit;
    private TextView mCupIdText;
    private TextView mDeviceInfoText;
    private ImageView mCupImage;
    private SeekBar mContrastSeekBar;
    private ProgressDialog mProgressDialog;

    private Bitmap mImage;
    private int mContrast = ImageProperties.DEFAULT_CONTRACT;
    ScheduledFuture<?> senderHandler;
    private String mCupId  = "PAULIG_MUKI_466741";
    private MukiCupApi mMukiCupApi;
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://mukiimager.somee.com/")
            .build();

    ApiService service = retrofit.create(ApiService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mCupImage = (ImageView) findViewById(R.id.imageSrc);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading. Please wait...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mMukiCupApi = new MukiCupApi(getApplicationContext(), new MukiCupCallback() {
            @Override
            public void onCupConnected() {
                showToast("Cup connected");
            }

            @Override
            public void onCupDisconnected() {
                showToast("Cup disconnected");
            }

            @Override
            public void onDeviceInfo(DeviceInfo deviceInfo) {
                hideProgress();
                mDeviceInfoText.setText(deviceInfo.toString());
            }

            @Override
            public void onImageCleared() {
                showToast("Image cleared");
            }

            @Override
            public void onImageSent() {
                showToast("Image sent");
            }

            @Override
            public void onError(Action action, ErrorCode errorCode) {
//                showToast("Error:" + errorCode + " on action:" + action);
                Log.e("fuck", "Error:" + errorCode + " on action:" + action);
            }
        });
        //reset(null);
    }

    private void setupImage() {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap result = Bitmap.createBitmap(mImage);
                ImageUtils.convertImageToCupImage(result, mContrast);
                ImageUtils.convertImageToCupImage(result, mContrast);
                return result;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                mCupImage.setImageBitmap(bitmap);
                hideProgress();
            }
        }.execute();
    }

    public void crop(View view) {
        showProgress();
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
        mImage = ImageUtils.cropImage(image, new Point(100, 0));
        image.recycle();
        setupImage();
    }

    public void reset(View view) {
       if (senderHandler != null) {
            senderHandler.cancel(true);
        }
        clear(null);
    }

    public void pictures(View view) {

        if (senderHandler != null) {
            senderHandler.cancel(true);
        }

        final Runnable imageSender = new Runnable() {
            public void run() {
                System.out.println("start sending image");
                requestPictures();
            }
        };
        senderHandler = scheduler.scheduleAtFixedRate(imageSender, 0, 20, SECONDS);
    }

    public void quotes(View view) {

        if (senderHandler!=null) {
            senderHandler.cancel(true);
        }

        final Runnable imageSender = new Runnable() {
            public void run() {
                System.out.println("start sending image");
                requestQuotes();
            }
        };
        senderHandler = scheduler.scheduleAtFixedRate(imageSender, 0, 20, SECONDS);

    }

    public void todos(View view) {

        if (senderHandler!=null) {
            senderHandler.cancel(true);
        }
        requestTodos();
    }

    private void requestTodos() {
        final String serialNumber ="0003771";
        final String id = "PAULIG_MUKI_466741";

        new AsyncTask<String, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                String url = params[0];
                URL urlUrl;
                Bitmap image;
                try {
                    urlUrl = new URL(url);
                    image  = BitmapFactory.decodeStream(urlUrl.openConnection().getInputStream());

                } catch (Exception e) {
                    Log.e("DENIS_TAG", e.getMessage() + e.getStackTrace());
                    image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
                }
                return image;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                Bitmap mImage = ImageUtils.scaleBitmapToCupSize(bitmap);
                int mContrast = ImageProperties.DEFAULT_CONTRACT;
                Bitmap result = Bitmap.createBitmap(mImage);
                Log.d("ismutable",  " " + result.isMutable());
                Bitmap mutable = result.copy(Bitmap.Config.ARGB_8888, true);
                ImageUtils.convertImageToCupImage(mutable, mContrast);
                mCupImage.setImageBitmap(bitmap);
                mMukiCupApi.sendImage(mutable, new ImageProperties(mContrast), id);
            }
        }.execute("http://mukitodo.somee.com/Home/Get");


    }

    public void clear(View view) {
//        showProgress();
        mMukiCupApi.clearImage(mCupId);
    }

    public void requestPictures() {
        final String serialNumber ="0003771";
        final String id = "PAULIG_MUKI_466741";

        new AsyncTask<String, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                String url = params[0];
                URL urlUrl;
                Bitmap image;
                try {
                    urlUrl = new URL(url);
                    image  = BitmapFactory.decodeStream(urlUrl.openConnection().getInputStream());

                } catch (Exception e) {
                    Log.e("DENIS_TAG", e.getMessage() + e.getStackTrace());
                    image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
                }
                return image;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                Bitmap mImage = ImageUtils.scaleBitmapToCupSize(bitmap);
                int mContrast = ImageProperties.DEFAULT_CONTRACT;
                Bitmap result = Bitmap.createBitmap(mImage);
                Log.d("ismutable",  " " + result.isMutable());
                Bitmap mutable = result.copy(Bitmap.Config.ARGB_8888, true);
                ImageUtils.convertImageToCupImage(mutable, mContrast);
                mCupImage.setImageBitmap(bitmap);
                  mMukiCupApi.sendImage(mutable, new ImageProperties(mContrast), id);
            }
        }.execute("http://mukiimager.somee.com/Api/Image/Bash");



    }


    public void requestQuotes() {
        final String serialNumber ="0003771";
        final String id = "PAULIG_MUKI_466741";
        service.getRandomImageUrl().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    new AsyncTask<String, Void, Bitmap>() {
                        @Override
                        protected Bitmap doInBackground(String... params) {
                            String url = params[0];
                            URL urlUrl;
                            Bitmap image;
                            try {
                                urlUrl = new URL(url);
                                image  = BitmapFactory.decodeStream(urlUrl.openConnection().getInputStream());

                            } catch (Exception e) {
                                Log.e("DENIS_TAG", e.getMessage() + e.getStackTrace());
                                image = BitmapFactory.decodeResource(getResources(), R.drawable.test_image);
                            }
                            return image;
                        }

                        @Override
                        protected void onPostExecute(Bitmap bitmap) {
                            super.onPostExecute(bitmap);
                            Bitmap mImage = ImageUtils.scaleBitmapToCupSize(bitmap);
                            int mContrast = ImageProperties.DEFAULT_CONTRACT;
                            setupImage(mImage, mContrast);
                            bitmap.recycle();
                            Log.d("DenisCode", Thread.currentThread().getName());
                            mCupImage.setImageBitmap(mImage);
                            mMukiCupApi.sendImage(mImage, new ImageProperties(mContrast), id);
                        }
                    }.execute(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void setupImage(final Bitmap mImage,final int mContrast) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap result = Bitmap.createBitmap(mImage);
                ImageUtils.convertImageToCupImage(result, mContrast);
                return result;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {

            }
        }.execute();
    }


//    public void requestPictures(View view) {
//        String serialNumber = mSerialNumberEdit.getText().toString();
//        showProgress();
//        new AsyncTask<String, Void, String>() {
//            @Override
//            protected String doInBackground(String... strings) {
//                try {
//                    String serialNumber = strings[0];
//                    return MukiCupApi.cupIdentifierFromSerialNumber(serialNumber);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                mCupId = s;
//                mCupIdText.setText(mCupId);
//                hideProgress();
//            }
//        }.execute(serialNumber);
//    }
//
//    public void deviceInfo(View view) {
//        showProgress();
//        mMukiCupApi.getDeviceInfo(mCupId);
//    }

    private void showToast(final String text) {
        hideProgress();
        Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
    }

    private void showProgress() {
        mProgressDialog.show();
    }

    private void hideProgress() {
        mProgressDialog.dismiss();
    }
}
