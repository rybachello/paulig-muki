package com.muki;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by denis on 26.11.16.
 */
public interface ApiService  {
    @GET("Api/Image/_9gag")
    Call<ResponseBody> getRandomImageUrl();
}
